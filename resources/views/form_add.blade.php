@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>添加
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                           <!-- <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                            -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>

                        </div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">添加</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>添加</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#basicClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" role="form"  method="POST" action="{{ url('formtype', array($form_type_id)) }}/form">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="form_type_id" value="{{ $form_type_id }}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">说明</label>
                                            <input type="text" name="remark" placeholder="请输入说明" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标示</label>
                                            <input type="text" name="flag" placeholder="请输入字母数字的标示符" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">类型</label>
                                            <select class="form-control" name="type">
                                                <option value="1">单行文本框</option>
                                                <option value="2">多行文本框</option>
                                                <option value="3">单选按钮</option>
                                                <option value="4">多选按钮</option>
                                                <option value="5">下拉菜单</option>
                                                <option value="6">日期选择</option>
                                            </select>
                                            <textarea style="margin-top:10px; display: none;" name="value" id="value" class="form-control" placeholder="多个选项值，多个用“|”隔,请不要使用空格！"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">条件限制</label>
                                            <select class="form-control" name="condition">
                                                <option value="1">无限制</option>
                                                <option value="2">英文数字汉字</option>
                                                <option value="3">英文大小写字符</option>
                                                <option value="4">全数字</option>
                                                <option value="5">手机</option>
                                                <option value="6">邮箱</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">是否必填</label>
                                            <select class="form-control" name="level">
                                                <option value="1">选填</option>
                                                <option value="2">必填</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">是否显示</label>
                                            <select class="form-control" name="display">
                                                <option value="1">显示</option>
                                                <option value="2">隐藏</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">排序</label>
                                            <input type="text" name="sort" placeholder="输入数值越大排序越靠前" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">错误内容提示</label>
                                            <input type="text" name="error" placeholder="错误内容提示" class="form-control">
                                        </div>

                                        <button class="btn btn-info" type="submit">添加</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <script>
        $(function(){
            $("select[name='type']").change(function(){
                if($(this).val() == 3 || $(this).val() == 4 || $(this).val() == 5)
                {
                    $('#value').show();
                }
                else
                {
                    $('#value').hide();
                }
            });
        })
    </script>
@endsection
