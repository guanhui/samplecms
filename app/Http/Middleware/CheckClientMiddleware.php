<?php namespace App\Http\Middleware;

use App\Client;
use Closure;
use Illuminate\Support\Facades\Redirect;

class CheckClientMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(!Client::check())
        {
            return Redirect::to('/pc/user/login');
        }
		return $next($request);
	}

}
