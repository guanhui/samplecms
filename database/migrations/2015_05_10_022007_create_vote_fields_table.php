<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vote_fields', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('remark')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('condition')->nullable();  //条件
            $table->tinyInteger('level')->nullable();  //选填必填
            $table->tinyInteger('display')->nullable();  //是否显示
            $table->smallInteger('sort')->nullable();
            $table->string('error')->nullable();
            $table->integer('vote_type_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vote_fields');
	}

}
