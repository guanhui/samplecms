jQuery(function ($) {
    if ($(".slide-pic").length > 0) {
        var defaultOpts = { interval: 5000, fadeInTime: 300, fadeOutTime: 200 };
        var _titles = $("ul.slide-txt li");
        var _titles_bg = $("ul.op li");
        var _bodies = $("ul.slide-pic li");
        var _count = _titles.length;
        var _current = 0;
        var _intervalID = null;
        var stop = function () { window.clearInterval(_intervalID); };
        var slide = function (opts) {
            if (opts) {
                _current = opts.current || 0;
            } else {
                _current = (_current >= (_count - 1)) ? 0 : (++_current);
            };
            _bodies.filter(":visible").fadeOut(defaultOpts.fadeOutTime, function () {
                _bodies.eq(_current).fadeIn(defaultOpts.fadeInTime);			
               if($(this).attr('id') == 'l1')
			{
				 _bodies.eq(1).css('background-color','#9c0006');
			
				
				}
                        else	if($(this).attr('id') == 'l2')
			{
				_bodies.eq(2).css('background-color','#C34344');
				
				
				}
			else			if($(this).attr('id') == 'l3')
			{
				 _bodies.eq(3).css('background-color','#0a1042');
			
				
				}
			else			if($(this).attr('id') == 'l4')
			{
				 _bodies.eq(4).css('background-color','#2963AC');
				
				
				}
			else			if($(this).attr('id') == 'l5')
			{
				 _bodies.eq(0).css('background-color','#0D5292');
				
				
				}
            });

	_titles_bg.removeClass("cur").eq(_current).addClass("cur");
            _titles.removeClass("cur").eq(_current).addClass("cur");
			
        };
        var go = function () {
            stop();
            _intervalID = window.setInterval(function () { slide(); }, defaultOpts.interval);
        };
        var itemMouseOver = function (target, items) {
            stop();
            var i = $.inArray(target, items);
            slide({ current: i });
        };
_titles.hover(function () {
 if ($(this).attr('class')!= 'cur') { 
itemMouseOver(this, _titles); 
} 
else { 
stop(); }
 }, go);
        _bodies.hover(stop, go);
        
        go();
    }
});