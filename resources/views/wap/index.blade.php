<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<meta name="viewport" content="width=device-width,height=device-height,inital-scale=1.0,maximum-scale=1.0,user-scalable=no;">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<meta charset="utf-8">

<link href="{{ asset('wap/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('wap/css/iscroll.css') }}" rel="stylesheet" type="text/css" />
<style>
.banner img {width: 100%;}
</style>

<script type="text/javascript" src="{{ asset('wap/js/iscroll.js') }}"></script>
</head>

<body>

<div class="banner">
	<div id="wrapper">
		<div id="scroller">
			<ul id="thelist">
                @foreach($info['flash'] as $k=>$v)
				<li><p>{{ $v['attributes']['title'] }}</p><a href="javascript:void(0)"><img src="{{ asset($v['attributes']['flash']) }}" /></a></li>
				@endforeach
			</ul>
		</div>
	</div>

	<div id="nav">
		<ul id="indicator">
            @foreach($info['flash'] as $k=>$v)
			<li class="active" ></li>
            @endforeach
		</ul>
	</div>
	
</div>

<div class="nav">
	<ul>
        <li><a href="{{ url('web') }}">首页</a>
            @foreach($info['nav'] as $val)
        <li><a href="{{ url('categories', array($val['attributes']['id'])) }}">{{ $val['attributes']['name'] }}</a>
        </li>
        @endforeach
        @foreach($info['company_nav'] as $val)
            <li><a href="{{ url('company', array($val['attributes']['id'])) }}">{{ $val['attributes']['title'] }}</a>
            </li>
        @endforeach
	</ul>
</div>
<div class="txt">
	<p>最新动态</p>
	<ul>
        @foreach($info['company'] as $k=>$v )
		    <!--<li><a href="{{ url('category', array($v['attributes']['category_id'])) }}/posts/{{ $v['attributes']['id'] }}">{{ $v['attributes']['title'] }}</a></li>-->
		    <li><a href="#">{{ $v['attributes']['title'] }}</a></li>
        @endforeach
	</ul>
</div>
<script type="text/javascript">
var count = document.getElementById("thelist").getElementsByTagName("img").length;	

var count2 = document.getElementsByClassName("menuimg").length;
for(i=0;i<count;i++){
	document.getElementById("thelist").getElementsByTagName("img").item(i).style.cssText = " width:"+document.body.clientWidth+"px";
}
document.getElementById("scroller").style.cssText = " width:"+document.body.clientWidth*count+"px";

setInterval(function(){
	myScroll.scrollToPage('next', 0,400,count);
},3500 );

window.onresize = function(){ 
	for(i=0;i<count;i++){
		document.getElementById("thelist").getElementsByTagName("img").item(i).style.cssText = " width:"+document.body.clientWidth+"px";
	}
	document.getElementById("scroller").style.cssText = " width:"+document.body.clientWidth*count+"px";
} 
</script>

<div class="copyright"><br />Copyright © 2015-2016 <a href="#"></a></div>

</body>
</html>