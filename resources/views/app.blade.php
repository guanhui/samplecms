<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>企业管理系统 V 2.0</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loader-style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">

    <link href="{{ asset('assets/js/stackable/stacktable.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/stackable/responsive-table.css') }}" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
</head>

<body>   <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>




                <div id="logo-mobile" class="visible-xs">
                   <h1>Apricot<span>v1.3</span></h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">{{ Auth::user()->email }}<b class="caret"></b>
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="{{ url('setting') }}">
                                    <span class="entypo-user"></span>&#160;&#160;修改个人信息</a>
                            </li>
                            <li>
                                <a href="{{ url('web/setting') }}">
                                    <span class="icon-information"></span>&#160;&#160;修改网站信息</a>
                            </li>
                            <!--<li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>-->
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('logout') }}">
                                    <span class="icon-cross"></span>&#160;&#160; 退出</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
        <div id="logo">
         <h1>企业CMS<span>v2.0</span></h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>
        <div class="dark">
            <form action="#">
                <span>
                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="Search Menu..." autofocus="">
                </span>
            </form>
        </div>

        <div class="search-hover">
            <form id="demo-2">
                <input type="search" placeholder="Search Menu..." class="id_search">
            </form>
        </div>

        <div class="skin-part">
            <div id="tree-wrap">
                <div class="side-bar">
                    <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span>文章管理</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>
                        @foreach($info['article'] as $k => $article)
                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ url('contact',[$article->id]) }}/edit" title="{{ $article->title }}">
                                <i class="{{ $info['web_icon'][$k] }}"></i>
                                <span>{{ $article->title }}</span>

                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ url('flashes') }}" title="轮播图">
                                <i class="icon-gaming"></i>
                                <span>轮播图</span>
                            </a>
                        </li>
                        <!--<li>
                            <a class="tooltip-tip ajax-load" href="social.html" title="Social">
                                <i class="icon-feed"></i>
                                <span>加入我们</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="media.html" title="Media">
                                <i class="icon-camera"></i>
                                <span>联系我们</span>

                            </a>
                        </li>-->
                    </ul>

                    <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span>栏目管理</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>
                        @foreach($info['nav'] as $key=>$val)
                        <li>
                            <a class="tooltip-tip ajax-load" href="{{ url('categories', [$val->id]) }}/posts" title="{{ $val->name }}">
                                <i class="{{ $info['columns_icon'][$key] }}"></i>
                                <span>{{ $val->name }}</span>
                                <div class="noft-blue">{{ $val->sort }}</div>
                            </a>
                        </li>
                        @endforeach
                        <!--
                        <li>
                            <a class="tooltip-tip ajax-load" href="mail.html" title="Mail">
                                <i class="icon-mail"></i>
                                <span>公司标题</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="icon.html" title="Icons">
                                <i class="icon-preview"></i>
                                <span>公司内幕</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="#" title="Extra Pages">
                                <i class="icon-document-new"></i>
                                <span>公司打卡</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip " href="login.html" title="login">
                                <i class="icon-download"></i>
                                <span>登陆</span>
                            </a>
                        </li>
                        -->
                    </ul>

                    <ul id="menu-showhide" class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span>留言板</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>


                        <li>
                            <a class="tooltip-tip" href="{{ url('notes') }}" title="留言板">
                                <i class="icon-monitor"></i>
                                <span>留言板</span>
                            </a>
                        </li>

                    </ul>


                    <ul id="menu-showhide" class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span>高级功能</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>



                        <li>
                            <a class="tooltip-tip" href="{{ url('client') }}" title="会员管理">
                                <i class="icon-user"></i>
                                <span>会员管理</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('adminorder') }}" title="订单管理">
                                <i class="icon-graph-bar"></i>
                                <span>订单管理</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('diyformtype') }}" title="自定义表单">
                                <i class="icon-document-edit"></i>
                                <span>自定义表单</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('votetype') }}" title="投票管理">
                                <i class="icon-calendar"></i>
                                <span>投票管理</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END OF SIDE MENU -->

    @yield('content')


    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="{{ asset('assets/js/preloader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/load.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/main.js') }}"></script>
    <!-- /MAIN EFFECT -->



</body>

</html>
