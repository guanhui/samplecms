<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormType extends Model {

	public static $rules = array(
        'title' => 'required|alpha_dash',
        'flag' => 'required|alpha_dash'
    );

}
