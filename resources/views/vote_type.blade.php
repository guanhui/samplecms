@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-layout"></span>
                            <span>投票管理
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <!--<div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>-->


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">投票管理</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="tableStaticClose">
                            <div class="title-alt">
                                <h6>
                                    投票管理</h6>
                                <div class="titleClose"><a href="{{ url('votetype/create') }}">添加</a></div>
                            </div>

                            <div class="body-nest" id="tableStatic">

                                <section id="flip-scroll">

                                    <table class="table table-bordered table-striped cf">
                                        <thead class="cf">
                                            <tr>
                                                <td>编号</td>
                                                <th>标识</th>
                                                <th>简介</th>
                                                <th class="numeric">创建时间</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($vote as $v)
                                            <tr>
                                                <td>{{ $v->id }}</td>
                                                <td>{{ $v->name }}</td>
                                                <td class="numeric">{{ \Illuminate\Support\Str::limit($v->remark, 10) }}</td>
                                                <td>{{ $v['attributes']['created_at'] }}</td>
                                                <td class="numeric">
                                                    <a href="{{ url('votetype', array( $v->id )) }}/edit">修改</a>
                                                    <a href="{{ url('votes', array($v->id)) }}/field">输入表单项</a>
                                                   <!-- <a href="{{ url('field', array($v->id)) }}/high">统计</a>-->
                                                    <form action="{{ url('votetype', array( $v->id )) }}" method="post">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="submit" style="border: none; background: none;position:absolute; margin:-19px 0 0 97px" value="删除">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </section>
                                <?php  echo $vote->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /END OF CONTENT -->
        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection


