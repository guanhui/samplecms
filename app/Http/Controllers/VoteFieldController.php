<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vote;
use App\VoteField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VoteFieldController extends CmsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($type_id)
	{
		$field = VoteField::where('vote_type_id', $type_id)->paginate(10);
        return view('vote_field')->with('info', $this->info)->with('field', $field)->with('vote_id', $type_id);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type_id)
	{
        return view('vote_field_add')->with('vote_id', $type_id)->with('info', $this->info);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request,$type_id)
	{
        if(VoteField::create($request->all()))
        {
            return Redirect::to('votes/'.$type_id.'/field');
        }
        else
        {
            return redirect()->back();
        }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($type_id, $field_id)
	{
		$field = VoteField::find($field_id);
        return view('vote_field_edit')->with('info', $this->info)->with('field', $field)->with('vote_id', $type_id);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$type_id, $field_id)
	{
        $field = VoteField::find($field_id);
        $field->remark = $request->input('remark');
        $field->type = $request->input('type');
        $field->flag = $request->input('flag');
        $field->value = $request->input('value');
        $field->condition = $request->input('condition');
        $field->level = $request->input('level');
        $field->display = $request->input('display');
        $field->sort = $request->input('sort');
        $field->error = $request->input('error');
        $field->save();
        return Redirect::to('votes/'.$type_id.'/field');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($type_id, $field_id)
	{
        VoteField::find($field_id)->delete();
        return Redirect::to('votes/'.$type_id.'/field');
	}

    /**
     * 统计
     * @return $this
     */
    public function pie($field_id)
    {
        $vote = new Vote();
        $num = $vote->getVoteNum($field_id);  //统计数值

        $field = new VoteField();
        $text = $field->getFieldName($field_id);
        return view('pie')->with('info', $this->info)->with('vote', $num)->with('text', $text);
    }
}
