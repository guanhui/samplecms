<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiyFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('diy_forms', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('remark')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('condition')->nullable();
            $table->tinyInteger('level')->nullable();
            $table->tinyInteger('display')->nullable();
            $table->smallInteger('sort')->nullable();
            $table->string('error')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('diy_forms');
	}

}
