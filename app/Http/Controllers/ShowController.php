<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Post;
use App\Web;
use Illuminate\Http\Request;

class ShowController extends Controller {

    protected $info = array();
    protected $web_id;

    public function __construct()
    {
        $web = Web::first();
        $this->web_id = $web['attributes']['id'];
        $this->info['logo'] = $web['attributes']['logo'];
        $this->info['nav'] = Web::find($web['attributes']['id'])->categories()->where('display', '=', 1)->orderBy('sort', 'desc')->get();  //导航
        $company = array(
            'web_id' => $this->web_id,
            'display' => 1
        );
        $this->info['company_nav'] = Post::where($company)->get();
    }

    public function index()
    {

        return view('user.index')->with('info', $this->info);
    }


    public function category($category_id)
    {
        $categories = Category::find($category_id);
        $this->info['category_name'] = $categories['attributes']['name'];
        $this->info['category'] = $categories->posts()->paginate(10);
        return view('user.product')->with('info', $this->info);
    }


    public function company($post_id)
    {
        $result = Post::find($post_id);
        $this->info['company'] = $result['attributes'];
        return view('user.company')->with('info', $this->info);
    }


}
