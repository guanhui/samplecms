<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder{

    public function run()
    {
        Post::create([
            'title' => '公司介绍',
            'content' => '公司介绍',
            'web_id' => 1
        ]);

        Post::create([
            'title' => '联系我们',
            'content' => '联系我们',
            'web_id' => 1
        ]);

        Post::create([
            'title' => '人才招聘',
            'content' => '人才招聘',
            'web_id' => 1
        ]);
    }
}