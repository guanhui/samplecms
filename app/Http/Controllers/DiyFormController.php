<?php namespace App\Http\Controllers;

use App\DiyForm;
use App\FormType;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

/**
 * 输入自定义表单项
 * Class DiyFormController
 * @package App\Http\Controllers
 */

class DiyFormController extends CmsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($form_type_id)
	{
        $form = DiyForm::where('form_type_id', '=', $form_type_id)->paginate(10);
        return view('form')->with('info', $this->info)->with('form', $form)->with('form_type_id', $form_type_id);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($form_type_id)
	{
		return view('form_add')->with('info', $this->info)->with('form_type_id', $form_type_id)->with('web_id', $this->web_id);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request,$form_type_id)
	{
        if(DiyForm::create($request->all()))
        {
            return Redirect::to('formtype/'.$form_type_id.'/form');
        }

        return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($form_type_id, $form_id)
	{
		$form = DiyForm::find($form_id);
        return view('form_show')->with('info', $this->info)->with('form', $form);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($form_type_id, $form_id)
	{
        $form = DiyForm::find($form_id);
        return view('form_edit')->with('info', $this->info)->with('form', $form['attributes'])->with('form_type_id', $form_type_id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $form_type_id, $form_id)
	{
        $form = DiyForm::find($form_id);
        $form->remark = $request->input('remark');
        $form->type = $request->input('type');
        $form->value = $request->input('value');
        $form->level = $request->input('level');
        $form->display = $request->input('display');
        $form->sort = $request->input('sort');
        $form->error = $request->input('error');
        $form->save();
        return Redirect::to('formtype/'.$form_type_id.'/form');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($form_type_id, $form_id)
	{
        DiyForm::find($form_id)->delete();
        return Redirect::to('formtype/'.$form_type_id.'/form');
	}

}
