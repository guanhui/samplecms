<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Web extends Model {

	//
    public function categories()
    {
        return $this->hasMany('App\Category');
    }


    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function flashs()
    {
        return $this->hasMany('App\Flash');
    }
}
