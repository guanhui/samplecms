@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>修改
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                          <!--  <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                        -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                        </div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">修改</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>修改</h6>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('contact', array($info['contact']['id'])) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标题</label>
                                            <input type="text" name="title" placeholder="请输入标题" id="exampleInputEmail1" class="form-control" value="{{ $info['contact']['title'] }}">
                                        </div>


                                        <div class="form-group">
                                            <label for="exampleInputEmail1"></label>
                                            <textarea name="content" placeholder="请输入内容" id="editor" class="form-control" rows="10">{{ $info['contact']['content'] }}</textarea>
                                        </div>
<!--
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">状态</label>
                                            <div><input type="radio" name="display" @if($info['contact']['display']) checked @endif value="1"><span style="display: inline-block; margin-right: 10px;">显示</span><input type="radio" name="display" @if($info['contact']['display'] == 0) checked @endif value="0">隐藏</div>
                                        </div>
-->
                                        <button class="btn btn-info" type="submit">修改</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" />
    <script type="text/javascript" src="{{ asset('/simditor/scripts/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>
    <script>
        var editor = new Simditor({
            textarea: $('#editor'),
            upload: true
        });
    </script>
@endsection
