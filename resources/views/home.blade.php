@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-layout"></span>
                            <span>{{ $info['title'] }}
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <!--<div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>-->


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <!--<ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="{{ url('categories', array($info['category_id']))}}/display/{{ $info['display'] }}">
                                        @if($info['display'] == 1)
                                            <span class="entypo-heart margin-iconic"></span>关闭</a>
                                        @else
                                            <span class="entypo-heart margin-iconic"></span>显示</a>
                                        @endif
                                </li>-->
                              <!--  <li>
                                    <a href="#">
                                        <span class="entypo-cog margin-iconic"></span>Setting</a>
                                </li>
                            </ul>-->
                        </div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">{{ $info['title'] }}</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="tableStaticClose">
                            <div class="title-alt">
                                <h6>
                                    {{ $info['title'] }}<input type="text" id="categories-sort" class="noft-blue" style=" margin-left: 3px;width:30px;  background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);-webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;" value="{{ $info['sort'] }}"></h6>
                                <div class="titleClose"><a href="{{ url('categories', array($info['category_id'])) }}/posts/create">添加</a></div>
                                <!--<div class="titleClose">
                                    <a class="gone" href="#tableStaticClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#tableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>-->

                            </div>

                            <div class="body-nest" id="tableStatic">

                                <section id="flip-scroll">

                                    <table class="table table-bordered table-striped cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>编号</th>
                                                <th>标题</th>
                                                <th class="numeric">创建时间</th>
                                                <th class="numeric">修改时间</th>
                                                <th class="numeric">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($info['posts'] as $v)
                                            <tr>
                                                <td>{{ $v['attributes']['id'] }}</td>
                                                <td>{{ $v['attributes']['title'] }}</td>
                                                <td class="numeric">{{ $v['attributes']['created_at'] }}</td>
                                                <td class="numeric">{{ $v['attributes']['updated_at'] }}</td>
                                                <td class="numeric">
                                                    <a href="{{ url('categories', array($info['category_id'])) }}/posts/{{ $v['attributes']['id'] }}/edit">修改</a>
                                                    <form action="{{ url('categories', array($info['category_id'])) }}/posts/{{ $v['attributes']['id'] }}" method="post">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="submit" style="border: none; background: none;position:absolute; margin:-19px 0 0 27px" value="删除">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </section>
                                <?php  echo $info['posts']->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /END OF CONTENT -->
        <script>
            var categories_id = "{{ $info['category_id'] }}";
            $('#categories-sort').change(function(){
                var categories_sort = $(this).val();
                var token = $('#token').val();
                var data = {id:categories_id, sort: categories_sort, _token: token};
                $.post("{{ url('category/sort') }}", data, function(msg){
                },'json');
            });
        </script>
        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection


