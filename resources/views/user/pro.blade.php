@extends('user/app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">注册</div>
				<div class="panel-body">
                @foreach($product as $key=>$val)
                    <div>
                        {{ $val['attributes']['title'] }}<a href="{{ url('shopping', array($val['attributes']['id'])) }}">购买</a>
                    </div>
                @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
