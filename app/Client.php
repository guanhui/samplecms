<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Client extends Model {

    protected $fillable = ['name', 'email', 'tel', 'password'];

	public static $rules = array(
        'name' => 'required|alpha',
        'email' => 'required|email',
        'tel' => 'required|alpha_num',
        'password'=>'required|alpha_num|between:6,12|confirmed',
        'password_confirmation'=>'required|alpha_num|between:6,12'
    );

    /**
     * 检测客户是否登录
     * @return bool
     */
    public static function check()
    {
        if(Session::get('clientinfo'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 更新保存在session的用户信息
     */
    public static function setInfo()
    {
        Session::put('clientinfo', Client::find(Session::get('clientinfo')['id'])['attributes']);
    }

}
