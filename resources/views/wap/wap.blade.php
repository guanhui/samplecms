<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>标题</title>
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>

    <script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('wap/js/holder.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('wap/css/style.css') }}" />
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <a href="#">
                <img src="holder.js/100%x43/text:308x43 logo">
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <ul class="list1">
                <li><a href="{{ url('web') }}">首页</a>
                @foreach($info['nav'] as $val)
                    <li><a href="{{ url('categories', array($val['attributes']['id'])) }}">{{ $val['attributes']['name'] }}</a>
                </li>
                @endforeach
                @foreach($info['company_nav'] as $val)
                    <li><a href="{{ url('company', array($val['attributes']['id'])) }}">{{ $val['attributes']['title'] }}</a>
                    </li>
                @endforeach
               <li><a href="{{ url('note') }}">留言板</a></li>
            </ul>
        </div>
    </div>

    @yield('content')

    <div class="copyright"><br />Copyright © 2015-2016 <a href="#"></a></div>
</div>
</body>

</html>