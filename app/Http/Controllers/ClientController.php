<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CheckClientPutRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ClientController extends CmsController {


    public function index()
    {
        $client = Client::where('web_id', '=', $this->web_id)->paginate(10);
        return view('client')->with('client', $client)->with('info', $this->info);
    }

    /**
     * 客户信息
     * @return $this
     */
    public function ClientInfo()
    {
        $info = Session::get('clientinfo');
        return view('user.info')->with('info', $info);
    }

    /**
     * 显示修改客户信息界面
     * @return $this
     */
    public function EditInfo()
    {
        $info = Session::get('clientinfo');
        return view('user.info_edit')->with('info', $info);
    }

    /**
     * 客户信息修改处理
     * @param Request $request
     */
    public function EditInfoHandle(Request $request)
    {
        $client = Client::find(Session::get('clientinfo')['id']);
        $client->name = $request->input('name');
        $client->tel = $request->input('tel');
        $client->save();
        Client::setInfo();
        return Redirect::to('/pc/user/info');
    }

    /**
     * 显示修改密码
     * @return \Illuminate\View\View
     */
    public function EditPassword()
    {
        return view('user.password_edit');
    }

    /**
     * 修改密码处理
     */
    public function EditPasswordHandle(CheckClientPutRequest $request)
    {
        $client = Client::find(Session::get('clientinfo')['id']);
        $client->password = Hash::make($request->input('password'));
        $client->save();
        Client::setInfo();
        return Redirect::to('/pc/user/info');
    }
}
