<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'id' => 1,
            'email' => 'mengxiang@mx.com',
            'password' => Hash::make('mengxiang'),
        ]);
    }
}