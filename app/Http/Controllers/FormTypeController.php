<?php namespace App\Http\Controllers;

use App\FormType;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

/**
 * 自定义表单类型
 * Class FormTypeController
 * @package App\Http\CmsControllers
 */

class FormTypeController extends CmsController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $formtype = FormType::where('web_id', '=', $this->web_id)->paginate(10);
        return view('form_type')->with('info', $this->info)->with('formtype', $formtype);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('form_type_add')->with('info', $this->info);
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), FormType::$rules);
        if($validator->passes())
        {
            $formtyp = new FormType();
            $formtyp->title = $request->input('title');
            $formtyp->flag = $request->input('flag');
            $formtyp->web_id = $this->web_id;
            $formtyp->save();
            return Redirect::to('diyformtype');
        }
        else
        {
            return redirect()->back();
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request,$form_type_id)
	{
        $formtype = FormType::find($form_type_id);
        return view('form_type_edit')->with('info', $this->info)->with('formtype', $formtype['attributes']);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$form_type_id)
	{
        $validator = Validator::make($request->all(), FormType::$rules);
        if($validator->passes())
        {
            $formtype = FormType::find($form_type_id);
            $formtype->title = $request->input('title');
            $formtype->flag = $request->input('flag');
            $formtype->save();
            return Redirect::to('diyformtype');
        }
        else
        {
            return redirect()->back();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		FormType::find($id)->delete();
        return Redirect::to('diyformtype');
	}

}
