@extends('user/app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">注册</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('shopping', array($post['id'])) }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">商品</label>
							<table border="1">
                                <tr>
                                    <td>商品名称</td>
                                    <td>{{ $post['title'] }}</td>
                                </tr>
                                <tr>
                                    <td>商品价格</td>
                                    <td>{{ $post['price'] }}</td>
                                </tr>
                            </table>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">收货人</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="getter" value="{{ \Illuminate\Support\Facades\Session::get('clientinfo')['name']  }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">联系电话</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="gettel" value="{{ \Illuminate\Support\Facades\Session::get('clientinfo')['tel']  }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">地址</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address" value="请输入配送地址">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    提交订单
                                </button>
                            </div>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
