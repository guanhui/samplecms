<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    public function posts()
    {
        $this->belongsTo('App\Post');
    }

}
