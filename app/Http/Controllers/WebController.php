<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Facade;
use App\Http\Controllers\CmsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Web;
use App\User;
use Illuminate\Http\Request;

class WebController extends CmsController {

	public function edit()
    {
        $res = Web::Where('user_id', '=', Auth::user()->id)->get();
        $this->info['web'] = $res[0];
        return view('web')->with('info', $this->info);
    }

    /**
     * 更新操作
     * @param $web_id
     * @return mixed
     */
    public function update(Request $request, $web_id)
    {
        $res = Web::find($web_id);
        $res->title = Input::get('title');
        $res->description = Input::get('description');
        $res->keywords = Input::get('keywords');
        $res->phone = Input::get('phone');
        $res->address = Input::get('address');
        $res->code = Input::get('code');
        $res->lng = Input::get('lng');
        $res->lat = Input::get('lat');

        if($request->hasFile('logo') && $request->file('logo')->isValid())
        {
            $upload = $request->file('logo');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $file_name = md5($fileinfo['filename']).$fileinfo['dirname'].$fileinfo['extension'];
            $upload->move('upload', $file_name);
            $res->logo = 'upload/'.$file_name;
        }

        $res->save();
        return Redirect::to('categories/0/posts');
    }

    /**
     * 显示修改用户信息
     */
    public function UserEdit()
    {
        return view('user')->with('info', $this->info);
    }


    /**
     * 更新用户信息
     */
    public function UserUpdate(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $opassword = $request->input('opassword');
        if(!(Hash::check($opassword ,$user->password)))
        {
            return view('user')->with('info', $this->info);
        }

        if($request->input('password') != $request->input('repassword'))
        {
            return view('user')->with('info', $this->info);
        }

        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return Redirect::to('categories/0/posts');
    }
}
