<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0050)http://www.hantangsoft.com.cn/html/cpzs/index.html -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

      <title>产品展厅</title>
      <style type="text/css">
#bom a:link {
	color:white;
}
#bom a:visited {
	color:white;
}
#bom a:hover {
	color:#666;
}    
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('user/product_files') }}/css.css">
<script src="{{ asset('user/product_files') }}/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="{{ asset('user/product_files') }}/banner.js" type="text/javascript"></script>
<link href="{{ asset('user/product_files') }}/PagesCSS.css" rel="stylesheet" type="text/css">
<!--Created by Foosun Inc. 汉唐软件 at 2015-3-13 14:36:48--></head>

    <body>
        <!----main---->
        <div class="box_html"><!----head---->
        <div class="head">
        <div class="top">
        <div class="icon">
        <ul><li><img alt="" src="{{ asset('user/product_files') }}/icon_03_04.gif"><a href="#www.hantangsoft.com.cn/html/cpzs/index.html#">Email:吴先生,Kalin@hantangtech.com 、张先生,zxh@hantangsoft.com.cn</a></li>
<li><img alt="" src="{{ asset('user/product_files') }}/3.jpg"><a href="#www.hantangsoft.com.cn/html/cpzs/index.html#">联系电话：吴先生,0512-66383062-808、张先生,0512-66383062-816</a> </li>
<li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=109792952&site=qq&menu=yes"><img border="0" src="{{ asset('user/product_files') }}/pa" alt="点击这里给我发消息" title="点击这里给我发消息"></a></li> </ul>
            <div class="clr">&nbsp;</div>
            </div>
            </div>
            <!----daohang---->
            <div class="daohang">
            <div class="head_content">
            <div class="logo">&nbsp;</div>
<div class="ept1">&nbsp;</div>
	    <div class="ept2">&nbsp;</div>

            <div class="menu"><img class="menu_left" alt="" src="{{ asset('user/product_files') }}/daohang_left_16.gif">
            <ul>
                <li><img src="{{ asset('user/product_files') }}/daohang_line_21.gif"><a href="{{ url('pc') }}">首页</a></li>
                @foreach($info['nav'] as $key=>$val)
                    <li><img src="{{ asset('user/product_files') }}/daohang_line_21.gif"><a href="{{ url('pc/category', array($val['attributes']['id'] )) }}">{{ $val['attributes']['name'] }}</a></li>
                @endforeach

                @foreach($info['company_nav'] as $k=>$v)
                    <li><img src="{{ asset('user/product_files') }}/daohang_line_21.gif"><a href="{{ url('pc/company', array($v['attributes']['id'])) }}">{{ $v['attributes']['title'] }}</a></li>
                @endforeach
                </ul>

                <img class="menu_left" alt="" src="{{ asset('user/product_files') }}/daohang_right_16.gif"></div>
                <div class="clr">&nbsp;</div>
                </div>
                </div>
                </div>
                <!----middle---->
                <div class="middle"><!----middle_top---->
                <div class="middle_top"><!---- <div class="banner"></div>---></div>
                </div>
                <!----new---->
                <div class="new">
                <div class="news_left">
                <div class="news_list">
                <p>产品类目 <span>Products List</span></p>
                <ul>
 <li><a href="#www.hantangsoft.com.cn/html/2012-10/239.html">产品追踪追溯平台</a></li>
 <li><a href="#www.hantangsoft.com.cn/html/2012-10/238.html">汉唐设备管理系统</a></li>
 <li><a href="#www.hantangsoft.com.cn/html/2012-10/237.html">银行签报督办平台</a></li>
 <li><a href="#www.hantangsoft.com.cn/html/2012-10/236.html">协同办公管理系统</a></li>

 </ul>
                    <div class="clr">&nbsp;</div>
                    </div>
                    <div class="link"><div style="color:#333333;line-height:22px; font-size:12px"><p> 如何联系我们?</p>
技术总监：吴先生<br>

电话号码：0512-66383062-808<br>

手机号码：13338655168<br>

邮箱：Kalin@hantangtech.com <br>
</div>
<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=109792953&site=qq&menu=yes"><img border="0" src="{{ asset('user/product_files') }}/pa" alt="点击这里给我发消息" title="点击这里给我发消息"></a> <br>
 <!--<a href="#"><img src="/Templets/website/images/liuy_04.gif" /> 
<img src="/Templets/website/images/liuy_04.gif" /> </a>
<img src="/Templets/website/images/img_07_06.gif" />-->
<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=109792953&site=qq&menu=yes"><img border="0" src="{{ asset('user/product_files') }}/img_07_06.gif" alt="点击这里给我发消息" title="点击这里给我发消息"></a></div>
                    </div>

                    @yield('content')

                        <!----foot---->
                        <div class="footer">
                        <div class="footer_content"><div style="width:900px; height:100px; margin:0 auto">
    <div style=" width:800px; float:left; height:70px"><p>
版权所有  汉唐软件  
公司地址：苏州市吴中区木渎镇中山东路70号 吴中科技创业园2702室| 电话：0512-66383062-816，0512-88961321
    <br><a href="#www.jiutouchong.com/">九头虫</a>&nbsp;&nbsp;&nbsp;&nbsp;     <span>|</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#www.luntaione.com/">轮胎王</a>&nbsp;&nbsp;&nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#www.highchu.com/">小资厨房</a></p></div>
    <div style="width:800px; float:left; clear:left; height:30px">
<ul>
            <li><span>|</span><a href="#www.hantangsoft.com.cn/index.html">网站首页</a></li>
            <li><span>|</span><a href="#www.hantangsoft.com.cn/html/gmctd.html">关于我们</a></li>
            <li><span>|</span><a href="{{ asset('user/product_files') }}/product.html">产品展厅</a></li>
            <li><span>|</span><a href="#www.hantangsoft.com.cn/html/cpyc/index.html">人才招聘</a></li>
           <li><span>|</span><a href="#www.hantangsoft.com.cn/html/lqyss.html">联系我们</a></li>

</ul>
</div>
<div><a href="#www.szgswljg.gov.cn/getcompanyinfor.action?website1=hantangsoft.com.cn" target="_blank"> <img border="0" src="{{ asset('user/product_files') }}/szicb.gif"> </a></div>

</div></div>
                        <div class="clr">&nbsp;</div>
                        </div>
                        <div class="clr">&nbsp;</div>
                        </div>
                        <!----end---->
                        <div>&nbsp;</div>
                    
                












</body></html>