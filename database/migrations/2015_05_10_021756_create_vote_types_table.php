<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteTypesTable extends Migration {

	/**
     * 投票表单
     *
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vote_types', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 50)->nullable();  //标示
            $table->text('remark')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('sort')->default(0);
            $table->integer('web_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vote_types');
	}

}
