<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vote;
use App\VoteType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VoteTypeController extends CmsController {

	/**
     *
     * 表单投票
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $VoteType = VoteType::where('web_id', '=', $this->web_id)->paginate(10);
	    return view('vote_type')->with('info', $this->info)->with('vote', $VoteType);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('vote_type_add')->with('info', $this->info);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$vote = new VoteType();
        $vote->name = $request->input('name');
        $vote->remark = $request->input('remark');
        $vote->web_id = $this->web_id;
        $vote->sort = $request->input('sort');
        $vote->status = $request->input('status');
        $vote->save();
        return Redirect::to('votetype');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vote = VoteType::find($id);
        return view('vote_type_edit')->with('info', $this->info)->with('vote', $vote);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$vote = VoteType::find($id);
        $vote->name = $request->input('name');
        $vote->remark = $request->input('remark');
        $vote->status = $request->input('status');
        $vote->sort = $request->input('sort');
        $vote->save();
        return Redirect::to('votetype');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		VoteType::find($id)->delete();
        return Redirect::to('votetype');
	}


    public function high()
    {
        return view('high')->with('info', $this->info);
    }
}
