@extends('wap\wap')
@section('content')
    <div class="box2 clearfix">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('note') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="text" name="name" class="form-control" placeholder="昵称">
            <input type="text" name="tel" class="form-control" placeholder="手机号码">
            <input type="email" name="email" class="form-control" placeholder="电子邮箱">
        <!--	<input type="text" class="form-control" placeholder="手机号码">-->
            <textarea class="form-control" name="content" rows="3" placeholder="内容"></textarea>
            <br/>
            <button class="btn btn-default pull-right" type="submit">发送</button>
        </form>
    </div>
@endsection
