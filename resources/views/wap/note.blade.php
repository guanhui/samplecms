@extends('wap\wap')
@section('content')
    <div class="box1">
        <div class="row">
            <div class="col-xs-12">
                <div class="con">
                    <span><a href="{{ url('note/create') }}">我要留言</a></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="txt3">
                    <ul>
                        @foreach($info['note'] as $val)
                            <li><a href="#"><i class="glyphicon glyphicon-dashboard"></i>{{ $val['attributes']['name'] }}</a><span>{{ $val['attributes']['updated_at'] }}</span><br/><span><?php echo $val['attributes']['content'] ?></span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <?php echo $info['note']->render(); ?>
            </div>
        </div>
    </div>
@endsection