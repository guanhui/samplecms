<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * 客户登陆注册
 * Class MyController
 * @package App\Http\Controllers
 */
class MyController extends Controller {

    protected $web_id = 0;

    public function __construct()
    {
        $web = Web::get();
        $this->web_id = $web[0]['attributes']['id'];
    }

    public function postLogin(Request $request)
    {
        $client = Client::where('email', '=', $request->input('email'))->get();

        foreach($client as $key=>$val)
        {
            if(Hash::check($request->input('password'), $val['attributes']['password']))
            {
                Session::put('clientinfo', $client[$key]['attributes']);
                return Redirect::to('pc/user/info');
            }
        }

        return view('user.login');
    }

    public function postRegister(Request $request)
    {
        $validator = Validator::make($request->all(),Client::$rules);
        if($validator->passes())
        {
            $client = new Client();
            $client->name = $request->input('name');
            $client->tel = $request->input('tel');
            $client->email = $request->input('email');
            $client->password = Hash::make($request->input('password'));
            $client->web_id = $this->web_id;
            $client->save();
            return view('user.login');
        }
        else
        {
            return view('user.register');
        }
    }

    public function logout()
    {
        Session::pull('clientinfo');
        return Redirect::to('pc/user/login');
    }

}
