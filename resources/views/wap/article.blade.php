@extends('wap\wap')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="con">
                <span>{{ $info['article']['attributes']['title'] }}</span>
            </div>
        </div>
    </div>

    <div class="box1">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $info['article']['attributes']['content']; ?>
            </div>
        </div>
    </div>
@endsection