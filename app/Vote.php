<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

	public $fillable = ['field_id', 'value'];


    public function getVoteNum($field_id)
    {
        $field = new VoteField();
        $res = $field->getFieldValue($field_id);

        $arr = [];
        foreach($res as $key=>$val)
        {
            $condition = array(
                'field_id' => $field_id,
                'value' => $val
            );
            $arr[$key]['label'] = $val;
            $arr[$key]['value'] = Vote::where($condition)->count();
        }
        return $arr;
    }
}
