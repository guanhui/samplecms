<?php namespace App\Http\Controllers;

use App\Category;
use App\Flash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Note;
use App\Web;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class WapController extends Controller {

    protected $info = array();
    protected $web_id;
    public function __construct()
    {
        $web = Web::first();
        $this->web_id = $web['attributes']['id'];
        $this->info['logo'] = $web['attributes']['logo'];
        $this->info['nav'] = Web::find($web['attributes']['id'])->categories()->where('display', '=', 1)->orderBy('sort', 'desc')->get();  //导航

        $company = array(
            'web_id' => $this->web_id,
            'display' => 1
        );
        $this->info['company_nav'] = Post::where($company)->get();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    /**
     * 首页
     * @return $this
     */
	public function index()
	{
        $id = 0;
        foreach($this->info['nav'] as $key=>$val)
        {
            if($val['attributes']['name'] == '公司动态')
            {
                $id = $val['attributes']['id'];
            }
        }
        $this->info['company'] = Post::where('category_id', '=', $id)->take(5)->get();

        $this->info['flash'] = Flash::where('web_id', '=', $this->web_id)->get();

        return view('wap.index')->with('info', $this->info);
	}

	/**
     * 栏目列表  公司动态、产品中心、案例展示等
     */
    public function category($category_id)
    {
        $categories = Category::find($category_id);
        $this->info['category_name'] = $categories['attributes']['name'];
        $this->info['category'] = $categories->posts()->paginate(10);
        if($categories['attributes']['has_cover'])
        {
            return view('wap.product')->with('info', $this->info);
        }
        else
        {
            return view('wap.news')->with('info', $this->info);
        }
    }

    /**
     * 查看详细内容
     * @param $category_id
     * @param $posts_id
     */
    public function article($posts_id)
    {
        $this->info['article'] = Post::find($posts_id);
        return view('wap.article')->with('info', $this->info);
    }


    /**
     * 公司介绍、联系我们、人才招聘
     * @param $type
     * @return $this
     */
    public function company($posts_id)
    {
        $result = Post::find($posts_id);
        $this->info['company'] = $result['attributes'];
        return view('wap.company')->with('info', $this->info);
    }

    /**
     *  留言板
     * @return $this
     */
    public function note()
    {
        $this->info['note'] = Web::find($this->web_id)->notes()->paginate(5);
        return view('wap.note')->with('info', $this->info);
    }

    /**
     *  添加留言
     */
    public function addNote()
    {
        return view('wap.addnote')->with('info', $this->info);
    }

    /**
     * 添加留言处理
     * @param Request $request
     * @return mixed
     */
    public function addNoteHandle(Request $request)
    {
        $notes = new Note();
        $notes->tel = $request->input('tel');
        $notes->email = $request->input('email');
        $notes->name = $request->input('name');
        $notes->content = $request->input('content');
        $notes->status = 0;
        $notes->web_id = $this->web_id;
        $notes->save();
        return Redirect::to('note');
    }
}
