<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CmsController extends Controller {

    protected $info = array();
    protected $web_id = 0;

	public function __construct()
    {
        //栏目管理图标
        $this->info['columns_icon'] = array(
            'icon-user-group',
            'icon-view-thumb',
            'icon-monitor',
            'icon-download',
            'icon-media-shuffle',
        );

        $this->info['web_icon'] = array(
            'icon-tablet-landscape',
            'icon-phone',
            'icon-user'
        );

        $this->info['nav'] = DB::select("select * from categories where web_id=?",[Auth::user()->id]);
        $web = DB::table('webs')->where('user_id',Auth::user()->id)->first();
        $this->web_id = $web->id;
        $this->info['article'] = DB::table('posts')->where('web_id', $web->id)->get();
    }

    public function getColorTable()
    {
        $color = [
            [
                'color' => '#F7464A',
                'highlight' => '#FF5A5E'
            ],
            [
                'color' => '#46BFBD',
                'highlight' => '#5AD3D1'
            ],
            [
                'color' => '#FDB45C',
                'highlight' => '#FFC870'
            ],
            [
                'color' => '#949FB1',
                'highlight' => '#A8B3C5'
            ],
            [
                'color' => '#4D5360',
                'highlight' => '#616774'
            ],
            [
                'color' => '#CC6600',
                'highlight' => '#99CCCC'
            ],
            [
                'color' => '#FFFF00',
                'highlight' => '#FF6666'
            ],
            [
                'color' => '#FF0033',
                'highlight' => '#006699'
            ],
            [
                'color' => '#CCCC00',
                'highlight' => '#FFFF00'
            ],
            [
                'color' => '#990066',
                'highlight' => '#003399'
            ]
        ];

        return $color;
    }

}
