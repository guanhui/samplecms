@extends('user/app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">用户中心</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif


                        <div class="form-group">
                            <label class="col-md-4 control-label">用户名</label>
                            {{ $info['name'] }}

                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">联系方式</label>
                            {{ $info['tel'] }}
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">电子邮箱</label>
                            {{ $info['email'] }}
                        </div>


				</div>
			</div>
		</div>
	</div>
</div>
@endsection
