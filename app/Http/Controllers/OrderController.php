<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

/**
 * 订单处理
 * Class OrderController
 * @package App\Http\Controllers
 */

class OrderController extends CmsController {

	/**
     * 前台显示订单列表
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index()
    {
        $order = DB::table('orders')->join('clients', 'clients.id', '=', 'orders.client_id')->join('posts', 'posts.id', '=', 'orders.posts_id')->select('orders.*', 'clients.name', 'clients.tel', 'clients.email', 'posts.title', 'posts.price')->where('orders.client_id', '=', Session::get('clientinfo')['id'])->paginate(10);
        return view('user.order')->with('order', $order);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


    /**
     * 购买商品
     */
    public function shopping($post_id)
    {
        if(!Client::check()) return view('pc/user/login');
        $post = Post::find($post_id);
        return view('user.shopping')->with('post',$post['attributes']);
    }

    public function shoppingHandle(Request $request,$post_id)
    {
        if(!Client::check()) return view('pc/user/login');
        $order = new Order();
        $order->posts_id = $post_id;
        $order->client_id = Session::get('clientinfo')['id'];
        $order->order_id = $this->getToken(3).time();
        $order->getter = $request->input('getter');
        $order->gettel = $request->input('gettel');
        $order->address = $request->input('address');
        $order->save();
        return Redirect::to('client/order');
    }

    public function getToken($length = 18)
    {
        $chars = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'O', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z'
        );

        $token = '';
        for($i=0; $i < $length; $i++)
        {
            $token .= $chars[mt_rand(0, count($chars)-1)];

        }
        return $token;
    }
}
