<?php namespace App\Http\Controllers\Pc;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vote;
use App\VoteField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FormController extends Controller {

    protected $flag = '';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($vote_id)
	{
        $vote = VoteField::where('vote_type_id', '=', $vote_id)->get();
        $form = $this->getForm($vote);
        return view('user.form')->with('form', $form)->with('flag', $this->flag)->with('vote_id', $vote_id);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function FormHandle(Request $request, $vote_id)
    {
        $flag = explode(',',$request->input('flag'));
        foreach($flag as $key=>$val)
        {
            if(!empty($val))
            {
                $res = $this->getFormValue($request->input($val));

                if(is_array($res['value']))
                {
                    foreach($res['value'] as $v)
                    {
                        $vote = new Vote();
                        $vote->field_id = $res['field_id'];
                        $vote->value = $v;
                        $vote->save();
                    }

                }
                else
                {
                    $vote = new Vote();
                    $vote->field_id = $res['field_id'];
                    $vote->value = $res['value'];
                    $vote->save();
                }

            }
        }
        return Redirect::to('pc/vote');
    }

    /**
     * 对表单的值进行切割
     * @param $value
     */
    public function getFormValue($value)
    {
        $arr = [];
        if(is_array($value))
        {
            foreach($value as $k=>$v)
            {
                $temp = explode(',', $v);
                $arr['field_id'] = $temp[0];
                $arr['value'][] = $temp[1];
            }
        }
        else
        {
            $temp = explode(',', $value);
            $arr['field_id'] = $temp[0];
            $arr['value'] = $temp[1];
        }
        return $arr;
    }

    /**
     * 拼装表单
     * @param $vote
     */
    public function getForm($vote)
    {
        $arr = [];
        foreach($vote as  $key=>$val)
        {
            $this->flag .= $val->flag.',';
            $temp = explode("|",$val->value);

            if($val->type == 3)
            {
                $name = $val->flag;
                $type = 'radio';
            }
            else
            {
                $name = $val->flag.'[]';
                $type = 'checkbox';
            }

            $str = "<label>$val->remark</label>";
            foreach($temp as $k=>$v)
            {

                $str .= "<input type='$type' name='$name' value='$val->id,$v'>".$v;
            }
            $arr[] = $str;
        }
        return $arr;
    }
}
