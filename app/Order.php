<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {



    /**
     * 状态码装换
     * @param $status
     */
    public static function getStatus($status)
    {
        switch($status)
        {
            case 0:
                $result = '未发货';
                break;
            case 1:
                $result = '已发货';
                break;
            case 2:
                $result = '已完成';
                break;
            default:
                $result = '未发货';
        }
        return $result;
    }

}
