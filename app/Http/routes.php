<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/','UserController@login');

Route::get('logout', 'UserController@logout');

Route::post('user/postLogin', 'UserController@postLogin');

Route::group(['middleware'=>'auth'], function(){

    Route::resource('categories.posts', 'PostsController');
    Route::get('contact/{id}/edit', 'PostsController@contact');
    Route::put('contact/{id}', 'PostsController@contactUpdate');

    Route::get('web/setting', 'WebController@edit');
    Route::put('web/setting/{id}', 'WebController@update');

    Route::get('setting', 'WebController@UserEdit');
    Route::put('setting/handle', 'WebController@UserUpdate');

    Route::resource('notes', 'NoteController');
    Route::resource('flashes', 'FlashController');

    Route::get('flashes/{flashes_id}/{status}', 'FlashController@setStatus');

    Route::get('notes/{note_id}/{status}', 'NoteController@status');

    Route::get('categories/{categories_id}/display/{display}', 'PostsController@display');

    Route::get('client', 'ClientController@index');

    Route::resource('diyformtype', 'FormTypeController');

    Route::resource('formtype.form', 'DiyFormController');

    Route::resource('adminorder', 'AdminOrderController');

    Route::resource('votetype', 'VoteTypeController');  //投票表单

    Route::resource('votes.field', 'VoteFieldController');  //投票表单字段

    Route::get('field/{field}/pie', 'VoteFieldController@pie');

 //   Route::get('field/{field}/high', 'VoteTypeController@high');

    Route::get('adminorder/{id}/edit/{status}', 'AdminOrderController@setStatus');
});

Route::post('category/sort', 'PostsController@sort');
Route::get('categories/{category_id}/post/{posts_id}', 'ShowController@info');
Route::get('categories/{category_id}', 'ShowController@category');


/****************************WAP端start**************************************/

Route::get('web', 'WapController@index');

Route::get('categories/{category_id}', 'WapController@category');

Route::get('article/{post_id}', 'WapController@article');

Route::get('company/{id}', 'WapController@company');

Route::get('note', 'WapController@note');

Route::get('note/create', 'WapController@addNote');

Route::post('note', 'WapController@addNoteHandle');

/****************************WAP端end***************************************/

/****************************PC端start***************************************/

Route::get('pc', 'ShowController@index');
Route::get('pc/category/{category_id}', 'ShowController@category');
Route::get('pc/company/{post_id}', 'ShowController@company');

Route::get('pc/vote', 'Pc\VoteController@index');
Route::resource('vote/{vote_id}/form', 'Pc\FormController');
Route::post('vote/{vote_id}/handle', 'Pc\FormController@FormHandle');

/****************************PC端end*****************************************/


/****************************用户管理start***************************************/

Route::get('pc/user/login', function(){
    return view('user.login');
});

Route::get('pc/user/register', function(){
    return view('user.register');
});

Route::post('pc/user/postLogin', 'MyController@postLogin');

Route::post('pc/user/postRegister', 'MyController@postRegister');

Route::get('pc/user/logout', 'Mycontroller@logout');

Route::group(['middleware'=>'client'], function(){

    Route::get('pc/user/info', 'ClientController@ClientInfo');

    Route::get('pc/user/info/edit', 'ClientController@EditInfo');

    Route::put('pc/user/info', 'ClientController@EditInfoHandle');

    Route::get('pc/user/password/edit', 'ClientController@EditPassword');

    Route::put('pc/user/password', 'ClientController@EditPasswordHandle');

    Route::resource('order', 'ProductController');

    Route::get('shopping/{posts_id}', 'OrderController@shopping');

    Route::post('shopping/{posts_id}', 'OrderController@shoppingHandle');

    Route::get('client/order', 'OrderController@index');
});

/****************************用户管理end*****************************************/