<?php
/**
 * Created by PhpStorm.
 * User: mengxiang
 * Date: 2015/4/24
 * Time: 12:06
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Category;
class CategoryTableSeeder extends Seeder{

    public function run()
    {
        DB::table('categories')->delete();
        $result[0]['id'] = 1;
        Category::create([
            'name' => '公司动态',
            'has_textarea' => 1,
            'has_cover' => 0,
            'web_id' => 1,
        ]);

        Category::create([
            'name' => '产品介绍',
            'has_textarea' => 1,
            'has_cover' => 1,
            'has_price' => 1,
            'web_id' => 1,
        ]);

        Category::create([
            'name' => '案例展示',
            'has_textarea' => 1,
            'has_cover' => 1,
            'web_id' => $result[0]['id'],
        ]);

        Category::create([
            'name' => '下载专区',
            'has_textarea' => 0,
            'has_cover' => 1,
            'web_id' => 1,
        ]);

        Category::create([
            'name' => '友情链接',
            'has_textarea' => 0,
            'has_cover' => 0,
            'web_id' => 1,
        ]);

    }
}