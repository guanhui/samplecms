@extends('wap\wap')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="con">
                <span>{{ $info['company']['title'] }}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <img src="holder.js/100%x150/text:100%x150 banner" />
        </div>
    </div>

    <div class="box1">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $info['company']['content'] ?>
            </div>
        </div>
    </div>
@endsection