<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\CmsController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PostsController extends CmsController {

    public function index($category_id)
    {
        $category_id = $category_id == 0 ? $this->info['nav'][0]->id : $category_id;
        $category = Category::find($category_id);
        $this->info['title'] = $category['attributes']['name'];
        $this->info['category_id'] = $category['attributes']['id'];
        $this->info['sort'] = $category['attributes']['sort'];
        $this->info['display'] = $category['attributes']['display'];
        $posts = Post::where('category_id', '=', $this->info['category_id'])->paginate(10);
        $this->info['posts'] = $posts;
        return view('home')->with('info', $this->info);
    }

    /**
     * 添加
     * @param $id
     * @return $this
     */
    public function create($category_id)
    {
        $res = Category::find($category_id);
        $this->info['category'] = $res['attributes'];
        return view('add')->with('info', $this->info);
    }

    /**
     * 保存（添加处理界面）
     * @return mixed
     */
    public function store(Request $request, $categories_id)
    {
        $posts = new Post();
        $posts->title = Input::get('title');
        $posts->content = Input::get('content');
        $posts->price = Input::get('price');

        if($request->hasFile('cover') && $request->file('cover')->isValid())
        {
            $upload = $request->file('cover');
            $file_info = pathinfo($upload->getClientOriginalName());
            $file_name = md5($file_info['filename']).$file_info['dirname'].$file_info['extension'];
            $upload->move('upload', $file_name);
            $posts->cover = $file_name;
        }

        $posts->category_id = $categories_id;
        $posts->save();
        return Redirect::to('categories/'.$categories_id.'/posts');
    }

    /**
     * 显示编辑页面
     * @param $categories_id
     * @param $posts_id
     * @return $this
     */
    public function edit($categories_id, $posts_id)
    {
        $category = Category::find($categories_id);
        $this->info['category'] = $category['attributes'];
        $posts = Post::find($posts_id);
        $this->info['posts'] = $posts['attributes'];
        return view('edit')->with('info', $this->info);
    }

    /**
     * @return mixed
     */
    public function update(Request $request, $categories_id, $posts_id)
    {
        $posts = Post::find($posts_id);
        $posts->title = Input::get('title');
        $posts->content = Input::get('content');
        $posts->price = Input::get('price');

        if($request->hasFile('cover') && $request->file('cover')->isValid())
        {
            $upload = $request->file('cover');
            $file_info = pathinfo($upload->getClientOriginalName());
            $file_name = md5($file_info['filename']).$file_info['dirname'].$file_info['extension'];
            $upload->move('upload', $file_name);
            $posts->cover = $file_name;
        }

        $posts->save();
        return Redirect::to('categories/'.$categories_id.'/posts');
    }

    /**
     *
     */
    public function destroy($category_id, $posts_id)
    {
        $posts = Post::find($posts_id);
        $posts->delete();
        return Redirect::to('categories/'.$category_id.'/posts');
    }

    /**
     * 编辑网站内容
     * @param $posts_id
     */
    public function contact($posts_id)
    {
        $result = Post::find($posts_id);
        $this->info['contact'] = $result['attributes'];
        return view('contact')->with('info', $this->info);
    }

    public function contactUpdate(Request $request,$posts_id)
    {
        $posts = Post::find($posts_id);
        $posts->title = $request->input('title');
    //    $posts->display = $request->input('display');
        $posts->content = $request->input('content');
        $posts->save();
        return Redirect::to('categories/0/posts');
    }

    /**
     * ajax改变顺序
     * @param Request $request
     * @return string
     */
    public function sort(Request $request)
    {
        $category_id = $request->get('id');
        $category = Category::find($category_id);
        $category->sort = $request->get('sort');
        $category->save();
        $info = array(
            'status' => 200,
            'message' => '修改成功'
        );
        return json_encode($info);
    }

    /**
     * 显示关闭
     */
    public function display($category_id, $display)
    {
        $category = Category::find($category_id);
        if(is_numeric($display))
        {
            $category->display = $display == 1 ? 0 : 1;
            $category->save();
        }
        return Redirect::to('categories/'.$category_id."/posts");
    }
}
