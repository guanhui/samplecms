@extends('app')
@section('content')
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=WXiySz3lWQ8E5To8BmH2GQTN"></script>
    <!--  PAPER WRAP -->
    <style>
        .lng{
            width: 100px;
            height: 31px !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            box-shadow: none !important;
            color: #C7D5E0 !important;
            font-size: 13px !important;
        }
    </style>
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>修改
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                          <!--  <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                        -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                        </div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">修改</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>修改</h6>
                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('web/setting', array($info['web']['id'])) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">

                                        <div class="form-group">
                                            <label for="exampleInputFile">网站logo</label>
                                            <input type="file" name="logo">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司名称</label>
                                            <input type="text" name="title" placeholder="请输入公司名称" class="form-control" value="{{ $info['web']['title'] }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">SEO简介</label>
                                            <textarea name="description" placeholder="请输入SEO简介" class="form-control" style="min-height: 50px">{{ $info['web']['description'] }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">SEO关键字</label>
                                            <input type="text" name="keywords" placeholder="请输入SEO关键字"  class="form-control" value="{{ $info['web']['keywords'] }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">联系方式</label>
                                            <input type="text" name="phone" placeholder="请输入联系方式"  class="form-control" value="{{ $info['web']['phone'] }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">地址</label>
                                            <input type="text" name="address" placeholder="请输入公司名称"  class="form-control" value="{{ $info['web']['address'] }}" onblur="getLng(this.value)">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">经度</label>
                                            <input type="text" class="lng" name="lng" id="lng" value="{{ $info['web']['lng'] }}">
                                            <label for="exampleInputEmail1">纬度</label>
                                            <input type="text" class="lng" name="lat" id="lat" value="{{ $info['web']['lat'] }}">
                                        </div>


                                        <button class="btn btn-info" type="submit">修改</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->
    <script>

        function getLng(keyword) {
            var myGeo = new BMap.Geocoder();
            // 将地址解析结果显示在地图上,并调整地图视野
            myGeo.getPoint(keyword, function(point){
                if (point) {
                    document.getElementById('lng').value = point.lng;
                    document.getElementById('lat').value = point.lat;
                }else{
                    alert("您选择地址没有解析到结果!");
                }
            });
        }

    </script>
@endsection
