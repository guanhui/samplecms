<?php namespace App\Http\Controllers\Pc;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\VoteType;
use App\Web;
use Illuminate\Http\Request;

class VoteController extends Controller {

    public $web_id = 0;

    public function __construct()
    {
        $web = Web::first();
        $this->web_id = $web['attributes']['id'];
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $vote = VoteType::where('web_id', '=', $this->web_id)->paginate(10);
        return view('user.vote')->with('vote', $vote);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
