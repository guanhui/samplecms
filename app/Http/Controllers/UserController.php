<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {

    /**
     * 登录
     * @return \Illuminate\View\View
     */
    public function login()
    {
        return view('auth.login');
    }

    public function postLogin()
    {
        if(Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password'))))
        {
            return Redirect::to('categories/0/posts');
        }
        else
        {
            return Redirect::to('/');
        }
    }

    /**
     * 注册
     * @return \Illuminate\View\View
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * 注册处理
     */
    public function postRegister()
    {

    }

    /**
     * 注销
     * @return mixed
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}
