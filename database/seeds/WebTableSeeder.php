<?php
/**
 * Created by PhpStorm.
 * User: mengxiang
 * Date: 2015/4/24
 * Time: 11:50
 */
use Illuminate\Database\Seeder;
use App\Web;

class WebTableSeeder extends Seeder{

    public function run()
    {
        DB::table('webs')->delete();
        Web::create([
            'id' => 1,
            'title' => '梦享科技有限公司',
            'description' => '梦享科技，高端网络科技行业服务商。包含高端网站咨询与建设、app研发、微网站建设、微信公众号开发运营、软件应用定制开发、网络新媒体整合营销、美工设计等专业服务。',
            'keywords' => '梦享科技',
            'phone' => '(0755) 36616313',
            'address' => '广东省深圳市南山区阳光里A座2605',
            'code' => '518000',
            'user_id' => '1',
            'lng' => '113.934315',
            'lat' => '22.531392'
        ]);
    }
}
