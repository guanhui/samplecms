<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DiyForm extends Model {

	protected $fillable = ['remark', 'type', 'condition', 'value', 'level', 'display', 'sort', 'error', 'form_type_id', 'flag'];

    /**
     * 类型
     */
    public static function getType($type)
    {
        switch($type)
        {
            case 1:
                $resutl = '单行文本框';
                break;
            case 2:
                $resutl = '多行文本框';
                break;
            case 3:
                $resutl = '单选按钮';
                break;
            case 4:
                $resutl = '多选按钮';
                break;
            case 5:
                $resutl = '下拉菜单';
                break;
            case 6:
                $resutl = '日期选择';
                break;
        }
        return $resutl;
    }

    /**
     * 条件限制
     */
    public static function getCondition($condition)
    {
        switch($condition)
        {
            case 1:
                $resutl = '无限制';
                break;
            case 2:
                $resutl = '英文数字汉字';
                break;
            case 3:
                $resutl = '英文大小写字符';
                break;
            case 4:
                $resutl = '全数字';
                break;
            case 5:
                $resutl = '手机';
                break;
            case 6:
                $resutl = '邮箱';
                break;
        }
        return $resutl;
    }

    /**
     * 选填必填
     */
    public static function getLevel($level)
    {
        switch($level)
        {
            case 1:
                $resutl = '选填';
                break;
            case 2:
                $resutl = '必填';
                break;
        }
        return $resutl;
    }

    /**
     * 显示隐藏
     */
    public static function getDisplay($display)
    {
        switch($display)
        {
            case 1:
                $resutl = '显示';
                break;
            case 2:
                $resutl = '隐藏';
                break;
        }
        return $resutl;
    }

}
