<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebIdToDiyFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('diy_forms', function(Blueprint $table)
		{
			$table->integer('form_type_id')->nullable();
			$table->string('flag',20);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('diy_forms', function(Blueprint $table)
		{
			//
		});
	}

}
