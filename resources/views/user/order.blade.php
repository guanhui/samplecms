<style>
    table{
        width: 100%;
    }

    table tr{
        border-bottom: 1px solid #e8e8e8;
    }

    table tr th{
        padding: 10px 0;
        text-align: center;
        background: #e5e5e5;
        font-size: 14px;
        border-right: 1px solid #d5d5d5;
    }

    table tr td{
        padding: 10px 0;
        font-size: 12px;
        border-right: 1px solid #d5d5d5;
        text-align: center;
    }

    .imgtxt,.txt{
        float:right;
        margin-left: 5px;
    }
</style>
@extends('user\app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">文章列表</div>
				<div class="panel-body" style="padding: 0">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <th>订单号</th>
                            <th>收件人</th>
                            <th>收件电话</th>
                            <th>收件地址</th>
                            <th>商品名称</th>
                            <th>购买时间</th>
                            <th>状态</th>
                        </tr>
                        @foreach($order as $val)
                            <tr>
                                <td>{{ $val->order_id }}</td>
                                <td>{{ $val->getter }}</td>
                                <td>{{ $val->gettel }}</td>
                                <td>{{ $val->address }}</td>
                                <td>{{ $val->title }}</td>
                                <td>{{ $val->created_at }}</td>
                                <td>{{ \App\Order::getStatus($val->status) }}</td>
                            </tr>
                        @endforeach
                    </table>
				</div>
                <?php echo $order->render(); ?>
			</div>
		</div>
	</div>
</div>
@endsection
