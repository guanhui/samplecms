<form action="{{ url('vote', array($vote_id)) }}/handle" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="flag" value="{{ $flag }}">
    <table>
        <tr>
            <th>投票列表</th>
        </tr>
        @foreach($form as $key=>$val)
            <tr>
                <td><?php echo $val ?></td>
            </tr>
        @endforeach
    </table>
    <input type="submit" value="提交">
</form>
