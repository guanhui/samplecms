@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-layout"></span>
                            <span>首页轮播图
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <!--<div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>-->


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="btn-group btn-wigdet pull-right visible-lg">
                            <div class="btn">
                                设置</div>
                            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                        </div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">首页</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">轮播图</a>
                </li>
                <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="tableStaticClose">
                            <div class="title-alt">
                                <h6>
                                    首页轮播图</h6>
                                <div class="titleClose"><a href="{{ url('flashes/create') }}">添加</a></div>
                                <!--<div class="titleClose">
                                    <a class="gone" href="#tableStaticClose">
                                        <span class="entypo-cancel"></span>
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#tableStatic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>-->

                            </div>

                            <div class="body-nest" id="tableStatic">

                                <section id="flip-scroll">

                                    <table class="table table-bordered table-striped cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>编号</th>
                                                <th>标题</th>
                                                <th class="numeric">图片地址</th>
                                                <th class="numeric">状态</th>
                                                <th class="numeric">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($info['flash'] as $v)
                                            <tr>
                                                <td>{{ $v['attributes']['id'] }}</td>
                                                <td>{{ $v['attributes']['title'] }}</td>
                                                <td>{{ asset($v['attributes']['flash']) }}</td>
                                                <td>
                                                    @if($v['attributes']['status'] == 1)
                                                        显示
                                                    @else
                                                        隐藏
                                                    @endif
                                                </td>
                                                <td class="numeric">
                                                    @if($v['attributes']['status'] == 1)
                                                        <a href="{{ url('flashes', array($v['attributes']['id'], 0)) }}">隐藏</a>
                                                    @else
                                                        <a href="{{ url('flashes', array($v['attributes']['id'], 1)) }}">显示</a>
                                                    @endif
                                                        <a href="{{ url('flashes', array($v['attributes']['id'])) }}/edit">编辑</a>
                                                    <form action="{{ url('flashes', array($v['attributes']['id'])) }}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="submit" style="border: none; background: none;position:absolute; margin:-19px 0 0 57px" value="删除">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </section>
                                <?php  echo $info['flash']->render(); ?>
                            </div>

                        </div>


                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection


