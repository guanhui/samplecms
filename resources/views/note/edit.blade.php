@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">修改</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('notes',array($info['notes']->id)) }}/posts">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="PUT">
						<div class="form-group">
                            <label class="col-md-4 control-label">姓名</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ $info['notes']->name }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">标题</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="{{ $info['notes']->title }}">
                            </div>
                        </div>


						<div class="form-group">
							<label class="col-md-4 control-label">内容</label>
							<div class="col-md-6">
                                <textarea name="content" id="editor" class="form-control" rows="10">{{ $info['notes']->content }}</textarea>
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
                                    修改
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" />
<script type="text/javascript" src="{{ asset('/simditor/scripts/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
<script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
<script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
<script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>
<script>
    var editor = new Simditor({
        textarea: $('#editor'),
        upload: true
    });
</script>
@endsection
