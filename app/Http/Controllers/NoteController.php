<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Note;
use App\Web;
use Illuminate\Http\Request;
use App\Http\Controllers\CmsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class NoteController extends CmsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $web_id = Web::where('user_id', '=', Auth::user()->id)->get()[0]['attributes']['id'];
        $this->info['note'] = Note::where('web_id', '=', $web_id)->paginate(10);
        return view('note.home')->with('info', $this->info);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('note.add')->with('info', $this->info);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$notes = new Note();
        $notes->tel = $request->input('tel');
        $notes->email = $request->input('email');
        $notes->name = $request->input('name');
        $notes->content = $request->input('content');
        $notes->status = $request->input('status');
        $notes->web_id = Web::where('user_id', '=', Auth::user()->id)->get()[0]['attributes']['id'];
        $notes->save();
        return Redirect::to('notes');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $res = Note::find($id);
        $this->info['notes'] = $res['attributes'];
        return view('note.edit')->with('info', $this->info);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$res = Note::find($id);
        $res->title = $request->input('title');
        $res->name = $request->input('name');
        $res->content = $request->input('content');
        return Redirect::to('notes');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$notes = Note::find($id);
        $notes->delete();
        return Redirect::to('notes');
	}

    /**
     * 修改状态
     * @param $note_id
     * @param $status
     */
    public function status($note_id, $status)
    {
        $notes = Note::find($note_id);
        $notes->status = $status;
        $notes->save();
        return Redirect::to('notes');
    }

}
