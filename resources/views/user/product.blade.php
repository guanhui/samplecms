@extends('user\user')
@section('content')
<div class="news_right">
    <div class="bit">
        <p>{{ $info['category_name'] }}</p>
        <div class="x1">当前位置：<a href="{{ url('pc') }}">首页</a> &gt;&gt;  {{ $info['category_name'] }} </div>
    </div>
    <div class="news_right_list">
        <ul>
        @foreach($info['category'] as $key=>$val)
            <li><span>{{ $val['attributes']['updated_at'] }}</span><font>&gt;</font><a href="">{{ $val['attributes']['title'] }}</a> </li>
        @endforeach
            <div style="padding-top:15px;" class="foosun_pagebox">
                <?php echo $info['category']->render() ?>
            </div>
        </ul>
    </div>
    <div class="right_bottom">&nbsp;</div>
</div>
<div class="clr">&nbsp;</div>
</div>
@endsection
