<?php namespace App\Http\Controllers;

use App\Flash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Web;
use Illuminate\Http\Request;
use App\Http\Controllers\CmsController;
use Illuminate\Support\Facades\Redirect;

class FlashController extends CmsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->info['flash'] = Web::find($this->web_id)->flashs()->paginate(10);
        return view('flash.home')->with('info', $this->info);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('flash.add')->with('info', $this->info);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$flash = new Flash();
        $flash->title = $request->input('title');
        $flash->web_id = $this->web_id;

        if($request->hasFile('flash') && $request->file('flash')->isValid())
        {
            $upload = $request->file('flash');
            $file_info = pathinfo($upload->getClientOriginalName());
            $fileName = md5($file_info['filename']).$file_info['dirname'].$file_info['extension'];
            $upload->move('upload/flash', $fileName);
            $flash->flash = 'upload/flash/'.$fileName;
        }

        $flash->save();
        return Redirect::to('flashes');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($flash_id)
	{
        $this->info['flash'] = Flash::find($flash_id);
        return view('flash.edit')->with('info', $this->info);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $flash_id)
	{
        $flash = Flash::find($flash_id);
        $flash->title = $request->input('title');

        if($request->hasFile('flash') && $request->file('flash')->isValid())
        {
            $upload = $request->file('flash');
            $file_info = pathinfo($upload->getClientOriginalName());
            $fileName = md5($file_info['filename']).$file_info['dirname'].$file_info['extension'];
            $upload->move('upload/flash', $fileName);
            $flash->flash = 'upload/flash/'.$fileName;
        }
        $flash->save();
        return Redirect::to('flashes');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($flash_id)
	{
        $result = Flash::find($flash_id)->delete();
        return Redirect::to('flashes');
	}


    public function setStatus($flashes_id, $status)
    {
        $flash = Flash::find($flashes_id);
        $flash->status = $status;
        $flash->save();
        return Redirect::to('flashes');
    }
}
