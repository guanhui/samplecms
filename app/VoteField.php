<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteField extends Model {

    protected $fillable = ['remark', 'type', 'condition', 'value', 'level', 'display', 'sort', 'error', 'vote_type_id', 'flag'];

    /**
     * 获取选项值
     * @param $field_id
     * @return mixed
     */
    public function getFieldValue($field_id)
    {
        $field = VoteField::find($field_id);
        return explode('|', $field['attributes']['value']);
    }

    /**
     * 获取字段说明
     * @param $field_id
     * @return mixed
     */
    public function getFieldName($field_id)
    {
        $field = VoteField::find($field_id);
        return $field['attributes']['remark'];
    }

}
