<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('posts_id');
            $table->integer('client_id');
            $table->string('order_id', 50)->nullable();
            $table->double('price')->nullable();
            $table->smallInteger('number')->default(1);
            $table->tinyInteger('status')->nullable();
            $table->string('ems',30)->nullable();
            $table->string('odd', 20)->nullable();
            $table->string('getter', 20)->nullable();
            $table->string('gettel', 11)->nullable();
            $table->string('address')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
