<?php namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Post;
use Illuminate\Http\Request;

class AdminOrderController extends CmsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
    {
        $order = DB::table('orders')->join('clients', 'clients.id', '=', 'orders.client_id')->join('posts', 'posts.id', '=', 'orders.posts_id')->select('orders.*', 'clients.name', 'clients.tel', 'clients.email', 'posts.title', 'posts.price')->paginate(10);
        return view('order')->with('info', $this->info)->with('order', $order);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $order = DB::table('orders')->join('clients', 'clients.id', '=', 'orders.client_id')->join('posts', 'posts.id', '=', 'orders.posts_id')->select('orders.*', 'clients.name', 'clients.tel', 'clients.email', 'posts.title', 'posts.price')->where('orders.id', '=', $id)->first();
        return view('order_show')->with('info', $this->info)->with('order', $order);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


    public function setStatus($id, $status)
    {
        $order = Order::find($id);
        $order->status = $status;
        $order->save();
        return redirect()->back();
    }
}
