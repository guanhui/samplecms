@extends('wap\wap')
@section('content')
    <div class="box1">
        <div class="row">
            <div class="col-xs-12">
                <div class="con">
                    <span>{{ $info['category_name'] }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="txt3">
                    <ul>
                        @foreach($info['category'] as $val)
                        <li><a href="{{ url('article', array($val['attributes']['id'])) }}"><i class="glyphicon glyphicon-dashboard"></i>{{ $val['attributes']['title'] }}</a><br><span>{{ $val['attributes']['updated_at'] }}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <?php echo $info['category']->render(); ?>
            </div>
        </div>
    </div>
@endsection