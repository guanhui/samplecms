@extends('wap\wap')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="con">
                <span>{{ $info['category_name'] }}</span>
            </div>
        </div>
    </div>

    <div class="box1" style="padding: 10px;">
        <div class="row">
            @foreach($info['category'] as $val)
                <div class="col-xs-4">
                    <div class="box1">
                        <img src="{{ asset('upload') }}/{{ $val['attributes']['cover'] }}" />
                        <p>{{ $val['attributes']['title'] }}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <?php echo $info['category']->render() ?>
    </div>
@endsection